$(window).on('mercury:ready', function() {
  var link = $('#mercury_iframe').contents().find('#mercury-save-helper');
  Mercury.saveUrl = link.data('save-url');
});

$(window).on('mercury:saved', function() {
  window.location = '/topics'
});
