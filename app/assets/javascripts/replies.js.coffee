# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
class Replies
  constructor: (replies_data) ->
    @topic_id = replies_data.topic_id
    @replies = replies_data.replies

  repliesHtml: (reply) ->
    htmlText = """
               <div class=reply-body>
                 <span class=reply-content>
                   #{reply}
                 </span>
               </div>
               """

  repliesInput: () ->
    submit = "回复"
    htmlText = """
                <form class="reply-form-#{@topic_id}">
                  <input type='text', class="reply-text reply-text-#{@topic_id}">
                  <button type='submit', class="reply-submit btn reply-submit-#{@topic_id}">#{submit}</button>
                </form>
               """

  appendReplies: () =>
    $.each @replies, (i, v) =>
      $(".replies-content-#{@topic_id}")
        .hide()
        .append(@repliesHtml(v.content))
        .fadeIn()

  appendInputBar: () ->
    $(".reply-input-#{@topic_id}")
      .hide()
      .append(@repliesInput())
      .fadeIn()

  appendReply: (data) ->  
    $(".replies-content-#{@topic_id}")
      .append(@repliesHtml(data.content))
    $(".reply-text-#{@topic_id}")
      .val("")

  bindReplyAction: () =>
    $(".reply-submit-#{@topic_id}").click (event) =>
      event.preventDefault()
      reply_data = {
        content: $(".reply-text-#{@topic_id}").val()
        topic_id: @topic_id
      }
      options = {
        type: 'POST'
        dataType: 'json'
        data: reply_data
        success: () =>
          @appendReply(reply_data)
        error: () ->
          alert("无法回复！")
      }
      url = "/replies"
      $.ajax(url, options)

  run: () =>
    replies_el = ".topic-replies-#{@topic_id}"
    expended = $(replies_el).data('expended')
    if expended
      $(".replies-content-#{@topic_id}").fadeOut().empty()
      $(".reply-input-#{@topic_id}").fadeOut().empty()
      $(replies_el).data("expended", false)
    else
      @appendReplies()
      @appendInputBar()
      @bindReplyAction()
      $(replies_el).data("expended", true)

$(document).ready ->
  $('.topic-replies').bind 'ajax:success', (event, data) ->
    replies = new Replies data
    replies.run()
