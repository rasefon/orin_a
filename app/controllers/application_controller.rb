class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :mercury_editor_path_helper

  protected

  def mercury_editor_path_helper(path)
    "/editor" + path
  end
  
end
