class RepliesController < ApplicationController

  def all
    respond_to do |format|
      replies = Topic.find(params[:id]).replies

      data_return = Hash.new
      data_return[:topic_id] = params[:id]
      data_return[:replies] = replies

      format.json { render :json => data_return.to_json }
    end 
  end

  def create
    respond_to do |format|
      reply = Topic.find(params[:topic_id]).replies.build(
        :user => current_user,
        :created_at => Time.now.utc,
        :updated_at => Time.now.utc,
        :content => params[:content]
      )
      #logger.info "#{current_user.gravatar_url(:size => 30)}\n"
      if reply.save
        format.json { render :text => '1', :status => 201 }
      else
        format.json { render :text => '0', :status => 501 }
      end  
    end
  end
end
