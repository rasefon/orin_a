class TopicsController < ApplicationController
  def index
    @topics = Topic.order('updated_at DESC')
  end
  
  def new
    @topic = Topic.new
  end
  
  def create
    @topic = current_user.topics.build
    @topic.content = params[:content][:topic_content][:value]

    begin
      @topic.save!
    rescue
    end

    render text: '{}'
  end

  def show
  end
  
  def edit
    @topic = Topic.find(params[:id])
  end

  def mercury_update
    @topic = Topic.find(params[:id])
    @topic.content = params[:content][:topic_content][:value]
    begin
      @topic.save!
    rescue
    end
    render text: '{}'
  end

  def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy

    respond_to do |format|
      format.html do
        redirect_to topics_url
      end
    end
  end

  private

end
