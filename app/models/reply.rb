class Reply < ActiveRecord::Base
  belongs_to :topic, :inverse_of => :replies
  belongs_to :user, :inverse_of => :replies
  attr_accessible :content, :created_at, :updated_at, :user

  validates :content, :user, :topic, :presence => true

end
