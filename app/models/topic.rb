class Topic < ActiveRecord::Base
  belongs_to :user
  attr_accessible :content, :edited_at, :title

  has_many :replies, :dependent => :destroy

  validates :content, :user, :presence => true
  
  def bind_images(ids)
    ids.each do |id|
      image = Mercury::Image.find(id)
      if image
        @logger ||= Logger.new("log/debug")
        @logger << mercury_images
        @logger << "\n"
      end
    end

  end
end
