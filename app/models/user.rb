class User < ActiveRecord::Base
  include Gravtastic
  gravtastic :secure => false, :size => 48, :rating => 'G'

  devise :database_authenticatable, :confirmable, :lockable, :recoverable,
    :rememberable, :registerable, :trackable, :timeoutable, :validatable,
    :token_authenticatable

  attr_accessible :email, :password, :password_confirmation, :username, :remember_me, :login

  has_many :topics, :dependent => :destroy
  has_many :replies, :dependent => :destroy
  
  validates :username, :presence => true
end
