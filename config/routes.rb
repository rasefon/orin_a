Orin::Application.routes.draw do
  Mercury::Engine.routes

  namespace :mercury do
    resources :images
  end

  mount Mercury::Engine => '/'

  devise_for :users

  root to: 'topics#index'

  resource :home, only: :index
  
  resources :replies, only: :create do
    member do
      get :all
    end
  end
  
  resources :topics do
    member do
      post :mercury_update
    end
  end
end
