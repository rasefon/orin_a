class RemoveTitleFromTopics < ActiveRecord::Migration
  def up
    remove_column :topics, :title
  end

  def down
    add_column :topics, :title, :string
  end
end
